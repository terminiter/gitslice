package cslicer.analyzer;

/*
 * #%L
 * CSlicer
 *    ______ _____  __ _                  
 *   / ____// ___/ / /(_)_____ ___   _____
 *  / /     \__ \ / // // ___// _ \ / ___/
 * / /___  ___/ // // // /__ /  __// /
 * \____/ /____//_//_/ \___/ \___//_/
 * %%
 * Copyright (C) 2014 - 2015 Department of Computer Science, University of Toronto
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import cslicer.builder.UnitTestScope;
import cslicer.utils.PrintUtils;
import cslicer.utils.PrintUtils.TAG;

public class ProjectConfiguration {

	public enum BUILD_SYSTEM {
		MAVEN, ANT
	}

	// default configurations
	public static final String DEFAULT_JDK = "1.7";
	public static final int DEFAULT_HISTORY_LENGTH = -1;
	public static final BUILD_SYSTEM DEFAULT_BUILD_SYSTEM = BUILD_SYSTEM.MAVEN;
	public static boolean DEFAULT_MAVEN_OUTPUT_ENABLED = false;
	public static boolean DEFAULT_OUTPUT_HUNK_GRAPH = false;
	public static final UnitTestScope DEFAULT_TEST_SCOPE = new UnitTestScope();
	public static final String DEFAULT_REPO_PATH = null;
	public static final String DEFAULT_START_COMMIT = null;
	public static final String DEFAULT_END_COMMIT = null;
	public static final String DEFAULT_BUILD_PATH = null;
	public static final String DEFAULT_JACOCO_PATH = null;
	public static final String DEFAULT_JAVA_SLICER_PATH = null;
	public static final String DEFAULT_SOURCE_ROOT = null;
	public static final String DEFAULT_CLASS_ROOT = null;
	public static final String DEFAULT_TOUCH_SET_PATH = null;
	public static final String DEFAULT_CALL_GRAPH_PATH = null;
	public static final Set<String> DEFAULT_EXCLUDED_PATH = Collections
			.emptySet();
	public static final Set<String> DEFAULT_SLICING_CRITERIA = Collections
			.emptySet();

	// private fields
	private String fRepoPath = DEFAULT_REPO_PATH;
	private String fStartId = DEFAULT_START_COMMIT;
	private String fEndId = DEFAULT_END_COMMIT; // optional - default HEAD
	private String fBuildPath = DEFAULT_BUILD_PATH;
	private boolean fOutputHunkGraph = DEFAULT_OUTPUT_HUNK_GRAPH;
	private String fSubModuleBuildPath = null; // optional
	private String fProjectPath = null; // optional
	private UnitTestScope fTestScope = DEFAULT_TEST_SCOPE; // optional
	private BUILD_SYSTEM fBuildSystem = DEFAULT_BUILD_SYSTEM; // optional
	private boolean fMavenOutputEnabled = DEFAULT_MAVEN_OUTPUT_ENABLED; // optional
	private String fJDKVersion = DEFAULT_JDK; // optional. default: 1.7
	/*
	 * optional. default -1. the number of commits to trace back from end if
	 * jacoco exec is provided then no need for build and module path, or tests
	 */
	private int fHistoryLength = DEFAULT_HISTORY_LENGTH;
	private String fTestJacocoExecPath = DEFAULT_JACOCO_PATH; // optional
	private String fSourceRootPath = DEFAULT_SOURCE_ROOT; // optional
	private String fClassRootPath = DEFAULT_CLASS_ROOT; // optional
	private Set<String> fExcludedPath = DEFAULT_EXCLUDED_PATH;
	private String fTouchSetPath = DEFAULT_TOUCH_SET_PATH; // optional
	private String fCallGraphPath = DEFAULT_CALL_GRAPH_PATH; // optional

	private String fJavaSlicerDumpPath = DEFAULT_JAVA_SLICER_PATH; // optional
	private Set<String> fJavaSlicerCriteria = DEFAULT_SLICING_CRITERIA; // optional

	public ProjectConfiguration() {
	}

	public ProjectConfiguration(Path configPath) {
		Properties config = new Properties();
		try {
			// load configurations from property file
			config.load(FileUtils.openInputStream(configPath.toFile()));

			this.setRepositoryPath(
					config.getProperty("repoPath", DEFAULT_REPO_PATH));
			this.setJacocoExecPath(
					config.getProperty("execPath", DEFAULT_JACOCO_PATH));
			this.setJavaSlicerDumpPath(
					config.getProperty("dumpPath", DEFAULT_JAVA_SLICER_PATH));
			Set<String> criteria = DEFAULT_SLICING_CRITERIA;
			criteria = new HashSet<String>(Arrays
					.asList(config.getProperty("criteria", "").split(",")));
			this.setJavaSlicerCreteria(criteria);
			this.setStartCommitId(
					config.getProperty("startCommit", DEFAULT_START_COMMIT));
			this.setEndCommitId(
					config.getProperty("endCommit", DEFAULT_END_COMMIT));
			this.setSourceRootPath(
					config.getProperty("sourceRoot", DEFAULT_SOURCE_ROOT));
			this.setClassRootPath(
					config.getProperty("classRoot", DEFAULT_CLASS_ROOT));
			this.setBuildScriptPath(
					config.getProperty("buildScriptPath", DEFAULT_BUILD_PATH));
			this.setTouchSetPath(
					config.getProperty("touchSetPath", DEFAULT_TOUCH_SET_PATH));
			this.setCallGraphPath(config.getProperty("callGraphPath",
					DEFAULT_CALL_GRAPH_PATH));
			this.setAnalysisLength(Integer.parseInt(config.getProperty(
					"historyLength", String.valueOf(DEFAULT_HISTORY_LENGTH))));
			this.setProjectJDKVersion(
					config.getProperty("jdkVersion", DEFAULT_JDK));

		} catch (IOException e) {
			PrintUtils.print("Error loading project configuration file at: "
					+ configPath, TAG.WARNING);
			e.printStackTrace();
		}
	}

	/**
	 * Return the length of history to be analyzed.
	 * 
	 * @return length
	 */
	public int getAnalysisLength() {
		if (fHistoryLength > 0)
			return fHistoryLength;
		else
			return 0;
	}

	/**
	 * Return path to project build script.
	 * 
	 * @return path
	 */
	public String getBuildScriptPath() {
		return fBuildPath;
	}

	public BUILD_SYSTEM getBuildSystem() {
		return fBuildSystem;
	}

	public String getCallGraphPath() {
		return fCallGraphPath;
	}

	public String getClassRootPath() {
		return fClassRootPath;
	}

	public String getEndCommitId() {
		return fEndId;
	}

	public Set<String> getExcludedPaths() {
		return fExcludedPath;
	}

	public String getJacocoExecPath() {
		return fTestJacocoExecPath;
	}

	public String getJavaSlicerDumpPath() {
		return fJavaSlicerDumpPath;
	}

	public boolean getOutputHunkGraph() {
		return fOutputHunkGraph;
	}

	public String getProjectJDKVersion() {
		return fJDKVersion;
	}

	/**
	 * The path to the project base directory. Use the root directory of the
	 * repository path by default.
	 * 
	 * @return a {@code String} representation of the project path
	 */
	public String getProjectPath() {
		if (fProjectPath == null)
			return FilenameUtils.getFullPath(
					FilenameUtils.normalizeNoEndSeparator(fRepoPath));
		return fProjectPath;
	}

	public String getRepositoryPath() {
		return fRepoPath;
	}

	public String getSourceRootPath() {
		return fSourceRootPath;
	}

	public String getStartCommitId() {
		return fStartId;
	}

	public String getSubModuleBuildScriptPath() {
		if (fSubModuleBuildPath == null)
			return fBuildPath;
		return fSubModuleBuildPath;
	}

	/**
	 * Return the names of interested test cases.
	 * 
	 * @return {@code null} if test suite is not set, then by default the whole
	 *         set is used
	 */
	public UnitTestScope getTestCases() {
		return fTestScope;
	}

	/**
	 * Return path to touch set dump file.
	 * 
	 * @return path
	 */
	public String getTouchSetPath() {
		return fTouchSetPath;
	}

	public boolean isClassRootPathSet() {
		return fClassRootPath != null;
	}

	public boolean isConsistent() {
		// repository path and start commit are required
		if (fRepoPath == null || (fStartId == null && fHistoryLength < 0)
				|| fBuildPath == null)
			return false;

		if ((fTestJacocoExecPath == null) != (fSourceRootPath == null)
				|| (fTestJacocoExecPath == null) != (fClassRootPath == null)
				|| (fClassRootPath == null) != (fSourceRootPath == null))
			return false;

		return true;
	}

	public boolean isJacocoExecPathSet() {
		return fTestJacocoExecPath != null;
	}

	public boolean isJavaSlicerDumpPathSet() {
		return fJavaSlicerDumpPath != null;
	}

	/**
	 * Get the project output setting.
	 * 
	 * @return {@code true} if output is enabled
	 */
	public boolean isBuilderOutputEnabled() {
		return this.fMavenOutputEnabled;
	}

	public boolean isSourceRootPathSet() {
		return fSourceRootPath != null;
	}

	public boolean isSubModuleSet() {
		return this.fSubModuleBuildPath != null;
	}

	public ProjectConfiguration setAnalysisLength(int len) {
		fHistoryLength = Math.max(0, len);
		return this;
	}

	public ProjectConfiguration setBuildScriptPath(String buildPath) {
		fBuildPath = FilenameUtils.normalize(buildPath);
		return this;
	}

	public ProjectConfiguration setBuildSystem(BUILD_SYSTEM buildSys) {
		this.fBuildSystem = buildSys;
		return this;
	}

	public ProjectConfiguration setCallGraphPath(String callPath) {
		fCallGraphPath = FilenameUtils.normalize(callPath);
		return this;
	}

	public ProjectConfiguration setClassRootPath(String classPath) {
		fClassRootPath = FilenameUtils.normalize(classPath);
		return this;
	}

	public ProjectConfiguration setEnableBuilderOutput(boolean enableOutput) {
		this.fMavenOutputEnabled = enableOutput;
		return this;
	}

	public ProjectConfiguration setEndCommitId(String commitId) {
		fEndId = commitId;
		return this;
	}

	public ProjectConfiguration setExcludedPaths(Set<String> excludes) {
		fExcludedPath = new HashSet<String>();
		for (String e : excludes) {
			fExcludedPath.add(FilenameUtils.normalize(e));
		}
		return this;
	}

	public ProjectConfiguration setJacocoExecPath(String execPath) {
		fTestJacocoExecPath = FilenameUtils.normalize(execPath);
		return this;
	}

	public ProjectConfiguration setJavaSlicerDumpPath(String dumpPath) {
		fJavaSlicerDumpPath = FilenameUtils.normalize(dumpPath);
		return this;
	}

	public ProjectConfiguration setOutputHunkGraph(boolean enable) {
		fOutputHunkGraph = enable;
		return this;
	}

	public ProjectConfiguration setProjectJDKVersion(String version) {
		fJDKVersion = version;
		return this;
	}

	public ProjectConfiguration setProjectPath(String projectPath) {
		this.fProjectPath = FilenameUtils.normalize(projectPath);
		return this;
	}

	public ProjectConfiguration setRepositoryPath(String repoPath) {
		fRepoPath = FilenameUtils.normalize(repoPath);
		return this;
	}

	public ProjectConfiguration setSourceRootPath(String sourcePath) {
		fSourceRootPath = FilenameUtils.normalize(sourcePath);
		return this;
	}

	public ProjectConfiguration setStartCommitId(String commitId) {
		fStartId = commitId;
		return this;
	}

	public ProjectConfiguration setSubModuleBuildScriptPath(String buildPath) {
		fSubModuleBuildPath = FilenameUtils.normalize(buildPath);
		return this;
	}

	public ProjectConfiguration setTestCases(UnitTestScope scope) {
		this.fTestScope = scope;
		return this;
	}

	public ProjectConfiguration setTouchSetPath(String path) {
		fTouchSetPath = FilenameUtils.normalize(path);
		return this;
	}

	public Set<String> getJavaSlicerCriteria() {
		return fJavaSlicerCriteria;
	}

	public ProjectConfiguration setJavaSlicerCreteria(Set<String> creteria) {
		this.fJavaSlicerCriteria = creteria;
		return this;
	}
}
