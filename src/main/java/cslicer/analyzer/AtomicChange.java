package cslicer.analyzer;

import org.eclipse.jgit.revwalk.RevCommit;

/*
 * #%L
 * CSlicer
 *    ______ _____  __ _                  
 *   / ____// ___/ / /(_)_____ ___   _____
 *  / /     \__ \ / // // ___// _ \ / ___/
 * / /___  ___/ // // // /__ /  __// /
 * \____/ /____//_//_/ \___/ \___//_/
 * %%
 * Copyright (C) 2014 - 2015 Department of Computer Science, University of Toronto
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import cslicer.analyzer.SlicingResult.DEP_FLAG;

/**
 * Meta information on an atomic change.
 * 
 * @author Yi Li
 *
 */
public class AtomicChange {

	/**
	 * We only consider 3 types of changes: insert, delete and update. All
	 * changes made to identifiers are considered as a delete followed by an
	 * insert automatically. All updates are only on the body contents.
	 * 
	 * @author Yi Li
	 *
	 */
	public enum CHG_TYPE {
		INS, DEL, UPD, /* SIG_UPD is kept for legacy reason */SIG_UPD;
	}

	private String fId;
	private String fPath;
	private RevCommit fChangeId;
	private DEP_FLAG fDepType;
	private CHG_TYPE fChangeType;

	public AtomicChange(String id, String path, RevCommit commit,
			DEP_FLAG depType, CHG_TYPE chgType) {
		fId = id;
		fPath = path;
		fChangeId = commit;
		fDepType = depType;
		fChangeType = chgType;
	}

	/**
	 * This {@link AtomicChange} should be kept.
	 * 
	 * @return {@code true} if changed entity is to be kept
	 */
	public boolean isKept() {
		// ignore delete
		if (fChangeType == CHG_TYPE.DEL)
			return false;

		// keep insert if it is in TEST or COMP
		if (fChangeType == CHG_TYPE.INS)
			return fDepType == DEP_FLAG.TEST || fDepType == DEP_FLAG.COMP;

		// now consider updates
		return (fDepType == DEP_FLAG.COMP && fChangeType == CHG_TYPE.SIG_UPD)
				|| fDepType == DEP_FLAG.TEST;
	}

	/**
	 * The dependency type of this {@link AtomicChange}.
	 * 
	 * @return {@link DEP_FLAG} of this {@link AtomicChange}
	 */
	public DEP_FLAG getDependencyType() {
		return fDepType;
	}

	/**
	 * The change type of this {@link AtomicChange}.
	 * 
	 * @return {@link CHG_TYPE} of this {@link AtomicChange}
	 */
	public CHG_TYPE getChangeType() {
		return fChangeType;
	}

	/**
	 * The path to the file that contains this {@link AtomicChange}.
	 * 
	 * @return file path
	 */
	public String getFilePath() {
		return fPath;
	}

	/**
	 * The identifier of the changed entity of this {@link AtomicChange}.
	 * 
	 * @return {@code String} identifier
	 */
	public String getIdentifier() {
		return fId;
	}

	/**
	 * The commits where this {@link AtomicChange} is extracted from.
	 * 
	 * @return {@link RevCommit} commit
	 */
	public RevCommit getCommit() {
		return fChangeId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "[" + fId + ":" + fChangeType + "," + fDepType + "]";
	}
}
