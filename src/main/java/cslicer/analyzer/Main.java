package cslicer.analyzer;

/*
 * #%L
 * CSlicer
 *    ______ _____  __ _                  
 *   / ____// ___/ / /(_)_____ ___   _____
 *  / /     \__ \ / // // ___// _ \ / ___/
 * / /___  ___/ // // // /__ /  __// /
 * \____/ /____//_//_/ \___/ \___//_/
 * %%
 * Copyright (C) 2014 - 2015 Department of Computer Science, University of Toronto
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FilenameUtils;
import org.codehaus.plexus.util.FileUtils;

import cslicer.CSlicer;
import cslicer.builder.BuildScriptInvalidException;
import cslicer.coverage.CoverageControlIOException;
import cslicer.coverage.CoverageDataMissingException;
import cslicer.jgit.AmbiguousEndPointException;
import cslicer.jgit.BranchNotFoundException;
import cslicer.jgit.CommitNotFoundException;
import cslicer.jgit.RepositoryInvalidException;
import cslicer.utils.PrintUtils;
import cslicer.utils.PrintUtils.TAG;
import cslicer.utils.StatsUtils;

/**
 * Main entry for the GitRef tool.
 * 
 * @author Yi Li
 *
 */
public class Main {

	public static void main(String[] args) {

		System.out.println("===== Git History Slicing Toolkit =====");
		System.out.println(CSlicer.CSLICER_LOGO);
		System.out.println("=======================================");

		// check environment
		if (!checkEnvVars()) {
			PrintUtils.print("Environment not setup properly. Abort.",
					TAG.WARNING);
			System.exit(1);
		}

		// create the command line parser
		CommandLineParser parser = new DefaultParser();
		// create the Options
		Options options = new Options();
		// options.addOption("r", "repo", true, "Path to target repository.");
		options.addOption("c", "config", true,
				"Path to project configuration file.");
		options.addOption("p", "print", false, "Output hunk dependency graph.");
		options.addOption("h", "help", false, "Print help messages.");
		options.addOption("q", "quiet", false, "No debug output.");
		options.addOption("v", "version", false,
				"Display version information.");

		try {
			CommandLine line = parser.parse(options, args);

			if (line.hasOption("version")) {
				displayVersionInfo();
			}

			if (line.hasOption("help")) {
				// automatically generate the help statement
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp("cslicer -c <CONFIG_FILE>", options);
			}

			if (line.hasOption("version") || line.hasOption("help"))
				System.exit(0); // terminate after showing help or version

			if (!line.hasOption("config")) {
				PrintUtils.print("Required argument '--config' is missing!",
						PrintUtils.TAG.WARNING);
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp("cslicer -c <CONFIG_FILE> [-h|-q|-v]",
						options);
				System.exit(1);
			} else {
				Path configPath = Paths.get(line.getOptionValue("config"));

				if (!FileUtils
						.fileExists(configPath.toString())
						|| !FilenameUtils.getExtension(configPath.toString())
								.equals("properties")) {
					PrintUtils.print(
							"The specified project configuration file path is not valid!",
							TAG.WARNING);
					System.exit(1);
				}

				if (line.hasOption("quiet"))
					PrintUtils.supressDebugMessages();

				// start doing slicing
				StatsUtils.resume("total.time");
				ProjectConfiguration config = new ProjectConfiguration(
						configPath);
				config.setOutputHunkGraph(line.hasOption("print"));

				Slicer refactor = new Slicer(config);
				refactor.doSlicing();
				StatsUtils.stop("total.time");
			}
		} catch (CommitNotFoundException | RepositoryInvalidException
				| AmbiguousEndPointException e) {
			PrintUtils.print(e.getMessage(), TAG.WARNING);
			e.printStackTrace();
			System.exit(1);
		} catch (ParseException | BuildScriptInvalidException
				| CoverageControlIOException | IllegalArgumentException e) {
			PrintUtils.print(e.getMessage(), TAG.WARNING);
			System.exit(2);
		} catch (IOException e) {
			PrintUtils.print("Property file is missing!", TAG.WARNING);
			e.printStackTrace();
			System.exit(3);
		} catch (ProjectConfigInvalidException e) {
			PrintUtils.print("Project configuration file is not valid!",
					TAG.WARNING);
			e.printStackTrace();
			System.exit(4);
		} catch (BranchNotFoundException e) {
			e.printStackTrace();
		} catch (CoverageDataMissingException e) {
			e.printStackTrace();
		}

		StatsUtils.print();
	}

	private static void displayVersionInfo() throws IOException {

		System.out.println(CSlicer.PROJECT_NAME + " " + CSlicer.PROJECT_VERSION
				+ " (" + CSlicer.BUILD_NUMBER + "; " + CSlicer.BUILD_TIMESTAMP
				+ ")");
		System.out.println("Maven home: " + CSlicer.SYSTEM_MAVEN_HOME);
		System.out.println("Java home: " + CSlicer.SYSTEM_JAVA_HOME);
		System.out.println("Built on: " + CSlicer.OS_NAME + ", "
				+ CSlicer.OS_VERSION + ", " + CSlicer.OS_ARCH);
		System.out.println("Java version: " + CSlicer.JAVA_VERSION + ", "
				+ CSlicer.JAVA_VENDER);
	}

	private static boolean checkEnvVars() {
		if (System.getenv("JAVA_HOME") == null) {
			PrintUtils.print("Variable 'JAVA_HOME' is not set",
					PrintUtils.TAG.WARNING);
			return false;
		} else if (System.getenv("M2_HOME") == null) {
			PrintUtils.print("Variable 'M2_HOME' is not set",
					PrintUtils.TAG.WARNING);
			return false;
		}

		return true;
	}
}
