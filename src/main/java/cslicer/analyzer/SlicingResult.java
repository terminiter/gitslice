package cslicer.analyzer;

/*
 * #%L
 * CSlicer
 *    ______ _____  __ _                  
 *   / ____// ___/ / /(_)_____ ___   _____
 *  / /     \__ \ / // // ___// _ \ / ___/
 * / /___  ___/ // // // /__ /  __// /
 * \____/ /____//_//_/ \___/ \___//_/
 * %%
 * Copyright (C) 2014 - 2015 Department of Computer Science, University of Toronto
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jgit.revwalk.RevCommit;

/**
 * Slicing result. Assign each commit with a flag.
 * 
 * @author Yi Li
 *
 */
public class SlicingResult {

	public enum DEP_FLAG {
		DROP, TEST, COMP, HUNK
	}

	private LinkedHashMap<RevCommit, DEP_FLAG> fCommits;

	public SlicingResult(List<RevCommit> history) {
		fCommits = new LinkedHashMap<RevCommit, DEP_FLAG>();
		for (RevCommit h : history)
			fCommits.put(h, DEP_FLAG.DROP);
	}

	public SlicingResult(List<RevCommit> history,
			Map<RevCommit, DEP_FLAG> labels) {
		fCommits = new LinkedHashMap<RevCommit, DEP_FLAG>();
		for (RevCommit h : history)
			fCommits.put(h, labels.get(h));
	}

	/**
	 * Add flag for a commit.
	 * 
	 * @param c
	 *            commit reference
	 * @param f
	 *            {@link DEP_FLAG} label
	 */
	public void add(RevCommit c, DEP_FLAG f) {
		fCommits.put(c, f);
	}

	/**
	 * Get label for a commit.
	 * 
	 * @param c
	 *            commit reference
	 * @return {@link DEP_FLAG} label
	 */
	public DEP_FLAG getLabel(RevCommit c) {
		return fCommits.get(c);
	}

	/**
	 * Get the whole labeled history.
	 * 
	 * @return labeled history
	 */
	public List<Pair<RevCommit, DEP_FLAG>> getLabeledHistory() {
		List<Pair<RevCommit, DEP_FLAG>> res = new LinkedList<Pair<RevCommit, DEP_FLAG>>();
		for (RevCommit c : fCommits.keySet()) {
			res.add(Pair.of(c, fCommits.get(c)));
		}
		return res;
	}

	/**
	 * The number of commits in original history.
	 * 
	 * @return length of original history
	 */
	public int getTotalCount() {
		return fCommits.size();
	}

	/**
	 * The number of commits dropped after slicing.
	 * 
	 * @return size of sliced history
	 */
	public int getDropCount() {
		int dropCount = 0;
		for (RevCommit a : fCommits.keySet())
			if (getLabel(a).equals(DEP_FLAG.DROP))
				dropCount++;

		return dropCount;
	}

	@Override
	public String toString() {
		StringBuilder res = new StringBuilder();

		int hunkDeps = 0, semDeps = 0;

		for (RevCommit a : fCommits.keySet()) {

			if (getLabel(a).equals(DEP_FLAG.TEST)
					|| getLabel(a).equals(DEP_FLAG.COMP))
				semDeps++;
			if (!getLabel(a).equals(DEP_FLAG.DROP))
				hunkDeps++;

			res.append(fCommits.get(a) + ": " + a.abbreviate(8).name() + " : "
					+ a.getShortMessage());
			res.append("\n");
		}

		float size = (float) fCommits.size();

		res.append("Reduction Rates: " + (size - semDeps) * 100 / size + "%\n");
		res.append(
				"Reduction Hunks: " + (size - hunkDeps) * 100 / size + "%\n");

		return res.toString();
	}
}