package cslicer.analyzer;

/*
 * #%L
 * CSlicer
 *    ______ _____  __ _                  
 *   / ____// ___/ / /(_)_____ ___   _____
 *  / /     \__ \ / // // ___// _ \ / ___/
 * / /___  ___/ // // // /__ /  __// /
 * \____/ /____//_//_/ \___/ \___//_/
 * %%
 * Copyright (C) 2014 - 2015 Department of Computer Science, University of Toronto
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.Stack;

import org.apache.commons.io.FileUtils;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.revwalk.RevCommit;

import ch.uzh.ifi.seal.changedistiller.model.entities.Delete;
import ch.uzh.ifi.seal.changedistiller.model.entities.Insert;
import ch.uzh.ifi.seal.changedistiller.model.entities.Move;
import ch.uzh.ifi.seal.changedistiller.model.entities.SourceCodeChange;
import ch.uzh.ifi.seal.changedistiller.model.entities.SourceCodeEntity;
import ch.uzh.ifi.seal.changedistiller.model.entities.Update;
import cslicer.analyzer.AtomicChange.CHG_TYPE;
import cslicer.analyzer.SlicingResult.DEP_FLAG;
import cslicer.builder.BuildScriptInvalidException;
import cslicer.builder.UnitTestScope;
import cslicer.builder.maven.MavenInvoker;
import cslicer.callgraph.BcelStaticCallGraphBuilder;
import cslicer.callgraph.CGNode;
import cslicer.callgraph.ClassPathInvalidException;
import cslicer.callgraph.StaticCallGraphBuilder;
import cslicer.coverage.CheckedCoverageAnalyzer;
import cslicer.coverage.CoverageControlIOException;
import cslicer.coverage.CoverageDataMissingException;
import cslicer.coverage.CoverageDatabase;
import cslicer.coverage.FullCoverageAnalyzer;
import cslicer.coverage.ICoverageAnalyzer;
import cslicer.coverage.TestFailureException;
import cslicer.distiller.ChangeDistillerException;
import cslicer.distiller.ChangeExtractor;
import cslicer.distiller.GitRefSourceCodeChange;
import cslicer.jgit.AmbiguousEndPointException;
import cslicer.jgit.BranchNotFoundException;
import cslicer.jgit.CheckoutFileFailedException;
import cslicer.jgit.CommitNotFoundException;
import cslicer.jgit.CreateBranchFailedException;
import cslicer.jgit.DeleteBranchFailedException;
import cslicer.jgit.JGit;
import cslicer.jgit.RepositoryInvalidException;
import cslicer.utils.BytecodeUtils;
import cslicer.utils.DependencyCache;
import cslicer.utils.PrintUtils;
import cslicer.utils.PrintUtils.TAG;
import cslicer.utils.StatsUtils;

/**
 * Implementations of the semantic slicing algorithms.
 * 
 * @author Yi Li
 * @since JDK1.7
 */
public class Slicer {
	public enum PRUNE_STRATEGY {
		NEW_TO_OLD, OLD_TO_NEW, SIGNIFICANCE, RANDOM
	}

	private static int fSnapCounter; // index for snap shot branch
	private JGit fJGit;
	private final String fRepoPath;
	private final String fClassRootPath;
	private final RevCommit fStart;
	private final RevCommit fEnd; // optional: could be null

	private List<RevCommit> fHistory;
	private final UnitTestScope fTests; // name of the target test cases

	private Stack<String> fSnapShots;
	private Hashtable<String, LinkedHashMap<RevCommit, RevCommit>> fCommitMap;

	private final MavenInvoker fCompiler;
	private final ICoverageAnalyzer fCoverage; // test coverage analyzer

	private StaticCallGraphBuilder fCallGraph; // call graph builder

	// private Hashtable<String, Integer> fSignificance; // significance level

	private Set<String> fExcludedPaths;
	// test touching set
	private TouchSet fTestTouchSet;
	private String fTouchSetPath;

	private final ProjectConfiguration fConfig;

	private LinkedList<RevCommit> A;
	private LinkedList<RevCommit> D;

	private Set<String> fIgnoreFilesTotal; // files in which changes ignored

	private VersionTracker fTracker; // track version of each changed entity
	private String fCallGraphPath; // path to call graph DOT file

	public Slicer(ProjectConfiguration config)
			throws RepositoryInvalidException, CommitNotFoundException,
			BuildScriptInvalidException, CoverageControlIOException,
			AmbiguousEndPointException, ProjectConfigInvalidException,
			BranchNotFoundException, CoverageDataMissingException {

		// load configurations
		if (!config.isConsistent())
			throw new ProjectConfigInvalidException();

		fConfig = config;

		fRepoPath = config.getRepositoryPath();
		fJGit = new JGit(this.fRepoPath);
		fEnd = fJGit.getCommit(config.getEndCommitId());

		// if optional history length is provided
		if (config.getAnalysisLength() > 0) {
			fHistory = fJGit.getCommitList(fEnd, config.getAnalysisLength(),
					true);
			fStart = fHistory.get(0);
		} else {
			fStart = fJGit.getCommit(config.getStartCommitId());
			// history is a list of commits from target.child -> head
			fHistory = fJGit.getCommitList(fStart, fEnd, true);
		}

		fTests = config.getTestCases();
		fExcludedPaths = config.getExcludedPaths();

		// initialization
		fSnapShots = new Stack<String>();
		fTestTouchSet = new TouchSet();
		fTouchSetPath = config.getTouchSetPath();
		fCallGraphPath = config.getCallGraphPath();

		fSnapShots.push(fJGit.getCurrentBranchName());
		fSnapCounter = 0;

		PrintUtils.print("Original |H|: " + fHistory.size());

		// initialize commit map
		fCommitMap = new Hashtable<String, LinkedHashMap<RevCommit, RevCommit>>();
		LinkedHashMap<RevCommit, RevCommit> mapping = new LinkedHashMap<RevCommit, RevCommit>();
		for (RevCommit h : fHistory) {
			mapping.put(h, h);
		}
		fCommitMap.put(fSnapShots.peek(), mapping);

		// setup compilation checker
		fCompiler = new MavenInvoker(config.getBuildScriptPath(),
				config.isBuilderOutputEnabled());

		// setup coverage analyzer
		if (config.isClassRootPathSet() && config.isSourceRootPathSet()) {
			// class root path is set by the user
			fClassRootPath = config.getClassRootPath();
			if (config.isJavaSlicerDumpPathSet()) {
				fCoverage = new CheckedCoverageAnalyzer(
						config.getJavaSlicerDumpPath(),
						config.getJavaSlicerCriteria());
			} else if (config.isJacocoExecPathSet())
				fCoverage = new FullCoverageAnalyzer(config.getJacocoExecPath(),
						config.getSourceRootPath(), config.getClassRootPath());
			else
				fCoverage = null;
		} else {
			if (config.isJavaSlicerDumpPathSet()) {
				fCoverage = new CheckedCoverageAnalyzer(fCompiler,
						config.getJavaSlicerDumpPath(),
						config.getJavaSlicerCriteria());
			} else {
				fCoverage = (config.isSubModuleSet())
						? new FullCoverageAnalyzer(fCompiler,
								config.getSubModuleBuildScriptPath())
						: new FullCoverageAnalyzer(fCompiler);
			}

			// class root path is set by Maven invoker
			fClassRootPath = fCompiler.getClassPath().getAbsolutePath();
		}

		// initialize lists to be used by the slicing algorithm
		D = new LinkedList<RevCommit>();
		A = new LinkedList<RevCommit>(fHistory);
		fTracker = new VersionTracker(A);
	}

	@SuppressWarnings("unused")
	private boolean checkCompile() {
		return fCompiler.checkCompilation();
	}

	/**
	 * Restore repository state. Cleanup generated temporary files. Can only be
	 * called once.
	 */
	public void cleanUp() {
		try {
			fCompiler.restoreBuildFile();
		} catch (IOException e) {
			PrintUtils.print("Build file not restored!", TAG.WARNING);
		}
		fCompiler.cleanUp();
	}

	private String commitSummary(RevCommit c) {
		return c.abbreviate(8).name() + " : " + c.getShortMessage();
	}

	private List<RevCommit> computeHunkDepSet(
			final Collection<RevCommit> cSet) {
		DependencyCache cache = new DependencyCache();

		int i = 0;
		for (RevCommit a : cSet) {
			if (cache.directDepsComputed(a)) {
				continue;
			} else {
				fJGit.findHunkDependencies(a, new HashSet<RevCommit>(A), cache,
						fExcludedPaths);
			}

			PrintUtils.printProgress("Computing Hunks: ",
					i++ * 100 / cSet.size());
		}

		PrintUtils.print(cache);
		if (fConfig.getOutputHunkGraph())
			cache.outputCacheToFile(
					FileUtils
							.getFile(FileUtils.getTempDirectory(),
									"cslicer-hunk-deps-graph.dot")
							.getAbsolutePath());

		List<RevCommit> res = new LinkedList<RevCommit>();
		Set<RevCommit> deps = cache.getUnSortedDeps();
		for (RevCommit h : fHistory) {
			if (deps.contains(h))
				res.add(h);
		}

		return res;
	}

	public List<RevCommit> computeHunkDepSetWithId(
			final Collection<String> cSet) {
		Set<RevCommit> set = new HashSet<RevCommit>();

		for (String id : cSet) {
			try {
				set.add(fJGit.getCommit(id));
			} catch (CommitNotFoundException e) {
				PrintUtils.print("Provided commit not found in the repo!",
						TAG.WARNING);
				return Collections.emptyList();
			}
		}
		return computeHunkDepSet(set);
	}

	private void computeTestTouchSet(boolean skip) {
		PrintUtils.print("Computing FUNC & COMP set ... ", TAG.OUTPUT);

		try {
			// do coverage analysis on the latest version
			PrintUtils.print("Running coverage analysis ...", TAG.OUTPUT);
			StatsUtils.resume("tests.time");
			CoverageDatabase store;
			if (fTests == null) {
				// test scope provided
				store = fCoverage.analyseCoverage();
			} else {
				// run specified tests
				store = fCoverage.analyseCoverage(fTests);
			}
			StatsUtils.stop("tests.time");

			// build static call graph and include compilation dependencies
			PrintUtils.print("Drawing static call graph ...");
			StatsUtils.resume("call.graph.time");

			fCallGraph = skip ? new BcelStaticCallGraphBuilder()
					: new BcelStaticCallGraphBuilder(fClassRootPath,
							store.getPartiallyCoveredClassNames());

			if (fCallGraphPath == null)
				fCallGraph.buildCallGraph();
			else if (FileUtils.getFile(fCallGraphPath).length() != 0)
				fCallGraph.loadCallGraph(fCallGraphPath);
			else {
				fCallGraph.buildCallGraph();
				fCallGraph.saveCallGraph(fCallGraphPath);
			}

			// fCallGraph.getCallGraph().printCallGraph();
			StatsUtils.stop("call.graph.time");

			// construct FUNC & TEST set
			PrintUtils.print("Adding to touch set ...");
			if (fTouchSetPath != null
					&& fTestTouchSet.loadFromFile(fTouchSetPath))
				return;

			for (SourceCodeEntity c : store.getAllRelevantEntities()) {
				if (!fCallGraph.getCallGraph().hasNode(c.getUniqueName()))
					continue;

				fTestTouchSet.addToTestSet(c.getUniqueName(), c);
				// compute compilation dependencies using static extended call
				// graph. filter generic type in unique names
				for (CGNode compDep : fCallGraph.getCallGraph()
						.getTransitiveSuccessors(BytecodeUtils
								.filterGenericType(c.getUniqueName()))) {
					fTestTouchSet.addToCompSet(compDep.getName());
				}
			}

		} catch (CoverageControlIOException | ClassPathInvalidException
				| TestFailureException e) {
			PrintUtils.print("Test touch computation failed!",
					PrintUtils.TAG.WARNING);
			e.printStackTrace();
		} finally {
			// clean up
			// fCompiler.cleanUp();
		}

		PrintUtils.print(fTestTouchSet.toString());
		if (fTouchSetPath != null)
			fTestTouchSet.saveToFile(fTouchSetPath);
	}

	@SuppressWarnings("unused")
	private LinkedHashMap<RevCommit, RevCommit> currentMapping() {
		assert(fCommitMap.keySet().contains(fSnapShots.peek()));
		assert(fCommitMap.get(fSnapShots.peek()) != null);
		return fCommitMap.get(fSnapShots.peek());
	}

	@SuppressWarnings("unused")
	private String currentSnapName() {
		return fSnapShots.peek();
	}

	/**
	 * Do slicing with the default options.
	 * 
	 * @return a list of {@link RevCommit} to drop
	 * @throws CommitNotFoundException
	 *             if provided commit cannot be found
	 */
	public List<RevCommit> doSlicing() throws CommitNotFoundException {
		return doSlicing(false, false);
	}

	/**
	 * Implementation of the CSLICER semantic slicing algorithm.
	 * 
	 * @param skipHunk
	 *            skip hunk dependency computation
	 * @param skipCallGraph
	 *            skip call graph construction
	 * @return a list of {@link RevCommit} to drop
	 * @throws CommitNotFoundException
	 *             if provided commit cannot be found
	 */
	public List<RevCommit> doSlicing(boolean skipHunk, boolean skipCallGraph)
			throws CommitNotFoundException {

		// compute touched source code entities by the test
		computeTestTouchSet(skipCallGraph);

		StatsUtils.resume("main.algo");

		Collections.reverse(A);
		PrintUtils.breakLine();
		PrintUtils.print("Initial |S| = " + A.size(), PrintUtils.TAG.OUTPUT);
		ChangeExtractor extractor = new ChangeExtractor(fJGit,
				fConfig.getProjectJDKVersion());

		// inspecting commits from newest to oldest
		int i = 0;
		PrintUtils.print("Analysing Commits ... ", TAG.OUTPUT);
		for (RevCommit c : A) {

			PrintUtils.print(
					"=== Inspecting commit: " + commitSummary(c) + " ===");

			Set<GitRefSourceCodeChange> changes;
			try {
				changes = extractor.extractChanges(c);
			} catch (ChangeDistillerException e) {
				e.printStackTrace();
				continue;
			}

			// this set is to grow by the entities deleted
			// i.e. the entity has to exist so that it can be deleted later
			Set<SourceCodeEntity> compGrowth = new HashSet<SourceCodeEntity>();

			for (GitRefSourceCodeChange gitChange : changes) {

				// get change distiller change
				SourceCodeChange change = gitChange.getSourceCodeChange();
				// get file path to changed entity
				String filePath = gitChange.getChangedFilePath();
				// unique identifier of changed entity
				String uniqueName = null;
				// dependency type (reason for keeping)
				DEP_FLAG depType = DEP_FLAG.DROP;
				// change operation type
				CHG_TYPE chgType = null;

				// testTouchSet is the set of field/method that are touched
				// by the tests
				// parent entity is field/method/class which contains the
				// change
				if (change instanceof Delete) {
					// do nothing for delete, since it shouldn't appear
					// in the touch set
					// XXX need to consider lookup changes here
					Delete del = (Delete) change;
					uniqueName = del.getChangedEntity().getUniqueName();
					depType = DEP_FLAG.DROP;
					chgType = CHG_TYPE.DEL;
					compGrowth.add(del.getChangedEntity());

				} else if (change instanceof Insert) {
					Insert ins = (Insert) change;
					uniqueName = ins.getChangedEntity().getUniqueName();

					if (fTestTouchSet.hitTestSet(uniqueName)) {
						depType = DEP_FLAG.TEST;
					} else if (fTestTouchSet.hitCompSet(uniqueName)) {
						depType = DEP_FLAG.COMP;
					}

					chgType = CHG_TYPE.INS;
				} else if (change instanceof Update) {
					Update upd = (Update) change;
					uniqueName = upd.getNewEntity().getUniqueName();

					// is signature updated?
					boolean signatureChange = !upd.getChangedEntity()
							.getUniqueName().equals(uniqueName);

					assert!signatureChange;

					// grow FUNC set
					if (fTestTouchSet.hitTestSet(uniqueName)) {
						// touchGrowth.add(change.getChangedEntity());
						depType = DEP_FLAG.TEST;
					}
					// XXX verify that change of signature is treated as add
					// and remove!
					else if (fTestTouchSet.hitCompSet(uniqueName)) {
						depType = DEP_FLAG.COMP;
					}

					chgType = CHG_TYPE.UPD;
					// chgType = signatureChange ? CHG_TYPE.SIG_UPD
					// : CHG_TYPE.BODY_UPD;
				} else if (change instanceof Move) {
					// shouldn't detect move for structure nodes
					assert false;
				} else
					assert false;

				// track this atomic change
				fTracker.trackAtomicChange(new AtomicChange(uniqueName,
						filePath, c, depType, chgType));
			}

			// grow touch set if the commit is affecting -- this is to make sure
			// deleted entity exists beforehand.
			if (fTracker.isKeeping(c)) {
				for (SourceCodeEntity g : compGrowth) {
					fTestTouchSet.addToTestSet(g.getUniqueName(), g);
				}
			} else {
				D.add(c); // drop set from newest to oldest
			}

			PrintUtils.print("");
			PrintUtils.printProgress("Slicing history: ", i++ * 100 / A.size());
		}

		StatsUtils.stop("main.algo");

		// ---------------------------------------------------------------
		// compute hunk dependencies
		// the target commit is the latest commit in history
		// ---------------------------------------------------------------
		PrintUtils.breakLine();
		PrintUtils.print("Analysing Hunk Dependency ... ", TAG.OUTPUT);
		Set<RevCommit> keep = new HashSet<RevCommit>(A);
		keep.removeAll(D);

		StatsUtils.resume("hunk.deps.time");
		List<RevCommit> hunkDeps = new ArrayList<RevCommit>();
		if (skipHunk)
			hunkDeps.addAll(keep);
		else
			hunkDeps = computeHunkDepSet(keep);

		StatsUtils.stop("hunk.deps.time");
		for (RevCommit d : Collections.unmodifiableList(D)) {
			if (hunkDeps.contains(d)) {
				PrintUtils.print("Hunk depends on: " + commitSummary(d)
						+ " : added back.");
				keep.add(d);
				StatsUtils.count("hunk.deps.set");
			}
		}

		fTracker.markHunkDependency(hunkDeps);

		// first stage result before hunk analysis
		if (fCallGraph != null)
			fTracker.computeSlicingResult(fCallGraph.getCallGraph());

		D.removeAll(keep);

		// ---------------------------------------------------------------
		// output semantic slicing summary
		// ---------------------------------------------------------------
		PrintUtils.breakLine();
		PrintUtils.print("Original |H| " + A.size());
		PrintUtils.print("Tracker:\n" + fTracker.toString(), TAG.DEBUG);
		PrintUtils.print("Results:\n" + fTracker.getSlicingResult(),
				TAG.OUTPUT);

		// final files to be reverted

		// clean up
		fJGit.cleanRepo();

		return Collections.unmodifiableList(hunkDeps);
	}

	/**
	 * Dummy {@code doSlicing} method where H' is given.
	 * 
	 * @param keep
	 *            sliced sub-history H'
	 * @return H' plus hunk dependencies
	 */
	public List<RevCommit> doSlicing(Set<String> keep) {
		List<RevCommit> hunkDeps = computeHunkDepSetWithId(keep);

		Set<String> hunkId = new HashSet<String>();
		for (RevCommit h : hunkDeps)
			hunkId.add(h.getName());

		List<RevCommit> drop = new LinkedList<RevCommit>();

		Collections.reverse(A);
		for (RevCommit a : A) {
			if (hunkId.contains(a.getName())) {
				PrintUtils.print("hunk: " + commitSummary(a));
				continue;
			}
			if (keep.contains(a.getName())) {
				PrintUtils.print("keep: " + commitSummary(a));
				continue;
			}

			drop.add(a);
			PrintUtils.print("Drop: " + commitSummary(a));
		}

		return drop;
	}

	private String freshSnapName() {
		return "SNAPSHOT" + (fSnapCounter++);
	}

	protected Set<String> getReducedSet() {
		Set<String> dIds = new HashSet<String>();
		for (RevCommit d : D)
			dIds.add(commitSummary(d));
		return dIds;
	}

	public final SlicingResult getSlicingReseult() {
		return fTracker.getSlicingResult();
	}

	/**
	 * Return a view of the partial history starting from {@code commit}
	 * inclusive.
	 * 
	 * @param commit
	 *            the starting {@code RevCommit}
	 * @return list of {@code RevCommit} in the remaining history
	 */
	private final List<RevCommit> getRemainingHistory(RevCommit commit) {
		int index = fHistory.indexOf(commit);
		if (index == -1)
			return Collections.emptyList();

		return Collections
				.unmodifiableList(fHistory.subList(index, fHistory.size()));
	}

	// /**
	// * @param commit
	// * target {@link RevCommit}
	// * @return the significance level computed so far for target commit
	// */
	// public int getSignificance(RevCommit commit) {
	// if (fSignificance.contains(commit.getName())) {
	// return fSignificance.get(commit.getName());
	// }
	// return 0;
	// }

	/**
	 * Map {@link RevCommit} to the duplicate on the current snapshot branch.
	 * 
	 * @param origCommit
	 *            {@code RevCommit} on the original branch
	 * @return corresponding commit on the latest snapshot branch
	 */
	private RevCommit resolveSnapCommit(RevCommit origCommit) {
		RevCommit res = origCommit;
		for (String snap : fSnapShots) {
			if (fCommitMap.get(snap).containsKey(res)
					&& fCommitMap.get(snap).get(res) != null)
				res = fCommitMap.get(snap).get(res);
		}
		return res;
	}

	private boolean tryPickCommits(List<RevCommit> toPick) {
		if (toPick.isEmpty())
			return true;

		try {
			PrintUtils.print("Begin picking commits ...");
			// create a new snapshot branch from parent of root
			Ref snap = fJGit.createNewBranch(freshSnapName(), fStart);
			fSnapShots.push(snap.getName());

			LinkedHashMap<RevCommit, RevCommit> mapping = new LinkedHashMap<RevCommit, RevCommit>();
			for (RevCommit p : toPick) {
				mapping.put(p, null);
			}

			if (!fJGit.pickCommitsToBranch(snap.getName(), mapping, false)) {
				undoRemoveCommit(false);
				return false;
			}

			fCommitMap.put(snap.getName(), mapping);

			PrintUtils.print("Finish picking commits ...");
			return true;
		} catch (CreateBranchFailedException e) {
			e.printStackTrace();
		}

		return false;
	}

	@SuppressWarnings("unused")
	private boolean tryRemoveCommit(List<RevCommit> toRemove) {
		if (toRemove.isEmpty())
			return true;

		// XXX assume the toRemove set is sorted from newest to oldest
		RevCommit root = resolveSnapCommit(toRemove.get(toRemove.size() - 1));
		// XXX assume only one parent
		RevCommit parent = fJGit.getParentCommits(root).get(0);

		try {
			PrintUtils.print("Begin removing commits ...");
			PrintUtils.print("Root for removal: " + root.getShortMessage());

			// create a new snapshot branch from parent of root
			Ref snap = fJGit.createNewBranch(freshSnapName(), parent);
			fSnapShots.push(snap.getName());

			LinkedHashMap<RevCommit, RevCommit> mapping = new LinkedHashMap<RevCommit, RevCommit>();
			for (RevCommit p : getRemainingHistory(root)) {
				mapping.put(p, null);
			}

			if (!fJGit.pickCommitsToBranch(snap.getName(), mapping,
					new HashSet<RevCommit>(toRemove))) {
				undoRemoveCommit(false);
				return false;
			}

			fCommitMap.put(snap.getName(), mapping);

			PrintUtils.print("Finishing remove commits ...");
			return true;
		} catch (CreateBranchFailedException e) {
			e.printStackTrace();
		}

		return false;
	}

	/**
	 * Revert selected files silently.
	 * 
	 * @return {@code true} if revert successes
	 */
	protected boolean tryRevertFiles() {
		try {
			fJGit.checkOutFiles(fIgnoreFilesTotal, fStart);
			return true;
		} catch (CheckoutFileFailedException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Undo commit removal and restore to the original(HEAD) state.
	 * 
	 * @param hard
	 *            reset repository hard
	 */
	protected void undoRemoveCommit(boolean hard) {
		String orighead = fSnapShots.pop();
		assert(fJGit.getCurrentBranchName().equals(orighead));

		if (hard)
			fJGit.resetHeadHard();

		fJGit.checkOutExistingBranch(fSnapShots.peek());
		try {
			fJGit.deleteBranch(orighead);
			PrintUtils.print("Undo " + orighead + " and go back to "
					+ fSnapShots.peek());
		} catch (DeleteBranchFailedException e) {
			PrintUtils.print("Delete of branch " + orighead + " has failed!");
			e.printStackTrace();
		}
	}

	/**
	 * Cherry-picking the computed subset. Verify that there is not conflict.
	 * 
	 * @param pick
	 *            set of {@link RevCommit} to pick
	 * @return {@code true} if slicing does not cause conflict.
	 */
	public boolean verifyResultPicking(List<RevCommit> pick) {
		// try remove commits
		if (!tryPickCommits(pick))
			return false;

		return true;
	}

	/**
	 * Verify that the sliced history passes the tests.
	 * 
	 * @param pick
	 *            set of {@link RevCommit} to pick
	 * @return {@code true} if the slice does not cause conflict and the tests
	 *         pass
	 */
	public boolean verifyResultTestPassing(List<RevCommit> pick) {
		// try remove commits
		if (!tryPickCommits(pick))
			return false;

		// revert files to be ignored
		// if (!tryRevertFiles())
		// return false;

		// run test
		// XXX assume junit is in the dependencies
		boolean pass = fCompiler.runSingleTest(fTests);
		fCompiler.cleanUp();

		return pass;
	}

	/**
	 * Verify the slicing results with given ID of dropped commits.
	 * 
	 * @param dropId
	 *            a list of commit ID
	 * @return {@code true} if test passes
	 */
	public boolean verifyResultWithId(List<String> dropId) {
		List<RevCommit> drop = new LinkedList<RevCommit>();

		for (String id : dropId) {
			try {
				drop.add(fJGit.getCommit(id));
			} catch (CommitNotFoundException e) {
				PrintUtils.print("Provided commit not found in the repo!",
						TAG.WARNING);
				return false;
			}
		}
		return verifyResultTestPassing(drop);
	}
}
