package cslicer.analyzer;

/*
 * #%L
 * CSlicer
 *    ______ _____  __ _                  
 *   / ____// ___/ / /(_)_____ ___   _____
 *  / /     \__ \ / // // ___// _ \ / ___/
 * / /___  ___/ // // // /__ /  __// /
 * \____/ /____//_//_/ \___/ \___//_/
 * %%
 * Copyright (C) 2014 - 2015 Department of Computer Science, University of Toronto
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.jgit.revwalk.RevCommit;

import cslicer.analyzer.AtomicChange.CHG_TYPE;
import cslicer.analyzer.SlicingResult.DEP_FLAG;
import cslicer.callgraph.CGNode;
import cslicer.callgraph.StaticCallGraph;
import cslicer.utils.BytecodeUtils;
import cslicer.utils.PrintUtils;

/**
 * Track the earliest possible version that a code entity should be updated to.
 * 
 * @author Yi Li
 *
 */
public class VersionTracker {

	private Map<RevCommit, Set<AtomicChange>> fChangesets;
	private Map<RevCommit, DEP_FLAG> fLabels;
	private final List<RevCommit> fHistory;
	private VersionComparator fVersionComparator;

	private final Map<String, List<AtomicChange>> fChangedEntities;

	private Map<String, FileTracker> fFileTrackers;

	protected VersionTracker(List<RevCommit> history) {
		fHistory = history;
		fChangesets = new LinkedHashMap<RevCommit, Set<AtomicChange>>();
		fLabels = new LinkedHashMap<RevCommit, DEP_FLAG>();
		fChangedEntities = new HashMap<String, List<AtomicChange>>();

		fFileTrackers = new HashMap<String, FileTracker>();
		// updated entity -> entities directly use it (need to be updated)
		// fEntityUsedBy = new HashMap<String, Set<String>>();

		for (RevCommit h : history) {
			fChangesets.put(h, new HashSet<AtomicChange>());
			fLabels.put(h, DEP_FLAG.DROP);
		}
	}

	/**
	 * Return computed slicing results.
	 * 
	 * @return {@link SlicingResult}
	 */
	public SlicingResult getSlicingResult() {
		return new SlicingResult(fHistory, fLabels);
	}

	/**
	 * Track an {@link AtomicChange}.
	 * 
	 * @param delta
	 *            basic information about an atomic change
	 */
	public void trackAtomicChange(AtomicChange delta) {

		if (!fChangesets.containsKey(delta.getCommit()))
			fChangesets.put(delta.getCommit(), new HashSet<AtomicChange>());

		fChangesets.get(delta.getCommit()).add(delta);
	}

	// before calling this method, make sure fChangedEntities is propagated with
	// all entities changed in the history.
	private Set<String> computeSideEffects(String uniqueName,
			final StaticCallGraph callGraph) {
		Set<String> res = new HashSet<String>();

		// look at nodes which reference the given entity
		for (CGNode node : callGraph.getIncomingNodes(
				BytecodeUtils.filterGenericType(uniqueName))) {
			res.add(node.getName());
		}

		// keep only those appeared in the history
		res.retainAll(fChangedEntities.keySet());

		// side-effects only occur on dropped entities
		for (String r : res) {
			for (AtomicChange c : fChangedEntities.get(r))
				assert c.getDependencyType() == DEP_FLAG.DROP;
		}

		return res;
	}

	/**
	 * Process gathered information and produce the history slice (after hunk
	 * dependencies are computed).
	 * 
	 * @param callGraph
	 *            static call graph
	 */
	public void computeSlicingResult(final StaticCallGraph callGraph) {

		for (RevCommit c : fHistory) {
			// collect changed entities
			collectChangedEntities(c);
		}

		for (RevCommit c : fHistory) {

			// analyze side effects of dropped entities
			if (getDependencyType(c) == DEP_FLAG.DROP) {

				// track versions
				for (AtomicChange delta : fChangesets.get(c)) {
					if (delta.getChangeType() == CHG_TYPE.SIG_UPD
							|| delta.getChangeType() == CHG_TYPE.INS) {

						// the set of entities which reference delta entity
						Set<String> sideEffects = computeSideEffects(
								delta.getIdentifier(), callGraph);

						PrintUtils.print("Side-effects: ");
						PrintUtils.print(sideEffects);

						String filePath = delta.getFilePath();
						if (!fFileTrackers.containsKey(filePath))
							fFileTrackers.put(filePath,
									new FileTracker(filePath));
						fFileTrackers.get(filePath).trackEntity(delta);
					}
				}
			}
		}

		for (String file : fFileTrackers.keySet()) {
			FileTracker fTracker = fFileTrackers.get(file);
			if (fTracker.isConsistent()) {
				PrintUtils.print(file + " : " + fTracker.getLatestVersion());
			} else {
				PrintUtils.print(file + " : inconsistent!");
			}
		}
	}

	// Atomic changes are stored as a map: RevCommit --> Set<AtomicChange>.
	// Now collect all changed entities in a commit and re-organize atomic
	// change in a map from Entity --> List<AtomicChange>.
	private void collectChangedEntities(RevCommit c) {
		for (AtomicChange delta : fChangesets.get(c)) {
			String key = delta.getIdentifier();
			if (fChangedEntities.get(key) == null)
				fChangedEntities.put(key, new LinkedList<AtomicChange>());
			fChangedEntities.get(key).add(delta);
		}
	}

	/**
	 * Mark all {@link RevCommit} in {@code keep} set as HUNK if it is
	 * previously labeled as DROP.
	 * 
	 * @param keep
	 *            a set of {@link RevCommit} to keep as HUNK dependencies
	 */
	public void markHunkDependency(Collection<RevCommit> keep) {
		markFuncDependency();
		for (RevCommit k : keep) {
			if (fLabels.get(k) == DEP_FLAG.DROP)
				fLabels.put(k, DEP_FLAG.HUNK);
		}
	}

	private void markFuncDependency() {
		// label commit type
		for (RevCommit h : fHistory) {
			fLabels.put(h, getDependencyType(h));
		}
	}

	/**
	 * Return if a change set should be kept.
	 * 
	 * @param commit
	 *            target {@link RevCommit}
	 * @return {@code true} if change set hits TEST or COMP set
	 */
	public boolean isKeeping(RevCommit commit) {
		Set<AtomicChange> changeset = fChangesets.get(commit);

		if (changeset == null)
			return false;

		for (AtomicChange c : changeset) {
			if (c.isKept())
				return true;
		}

		return false;
	}

	private DEP_FLAG getDependencyType(RevCommit commit) {
		Set<AtomicChange> changeset = fChangesets.get(commit);
		DEP_FLAG type = DEP_FLAG.DROP;

		if (changeset == null)
			return type;

		for (AtomicChange c : changeset) {
			if (c.getDependencyType().equals(DEP_FLAG.TEST))
				return DEP_FLAG.TEST;

			if (c.getDependencyType().equals(DEP_FLAG.COMP))
				type = DEP_FLAG.COMP;
		}

		return type;
	}

	public String toString() {
		String res = "[\n";
		for (RevCommit version : fChangesets.keySet()) {
			res += version.getName() + " : "
					+ fChangesets.get(version).toString() + "\n";
		}
		res += "]";

		return res;
	}

	/**
	 * Information about changed files.
	 * 
	 * @author Yi Li
	 *
	 */
	class FileTracker {
		private String filePath;
		private Map<String, EntityTracker> entityTracker;

		protected FileTracker(String path) {
			filePath = path;
			entityTracker = new HashMap<String, EntityTracker>();
		}

		protected void trackEntity(AtomicChange change) {
			if (!entityTracker.containsKey(change.getIdentifier()))
				entityTracker.put(change.getIdentifier(),
						new EntityTracker(change.getIdentifier()));
			entityTracker.get(change.getIdentifier())
					.trackVersion(new Version(change.getCommit().getName()));
			assert change.getFilePath().equals(filePath);
		}

		protected boolean isConsistent() {
			return collectVersions().size() <= 1;
		}

		public Set<Version> collectVersions() {
			Set<Version> versions = new HashSet<Version>();
			for (EntityTracker e : entityTracker.values()) {
				versions.add(e.getLatestVersion());
			}
			return versions;
		}

		protected Version getLatestVersion() {
			Set<Version> versions = collectVersions();
			return Collections.max(versions, fVersionComparator);
		}
	}

	/**
	 * Information about changed entities.
	 * 
	 * @author Yi Li
	 *
	 */
	class EntityTracker {
		private String fEntity;
		private Set<Version> versions;

		public EntityTracker(String entity) {
			fEntity = entity;
			versions = new HashSet<Version>();
		}

		public void trackVersion(Version v) {
			versions.add(v);
		}

		protected Version getEarlistVersion() {
			return Collections.min(versions, fVersionComparator);
		}

		protected Version getLatestVersion() {
			return Collections.max(versions, fVersionComparator);
		}

		protected String getEntityName() {
			return fEntity;
		}
	}

	/**
	 * Abstraction representing a version.
	 * 
	 * @author Yi Li
	 *
	 */
	class Version {
		private String fId;

		protected Version(String id) {
			fId = id;
		}

		public String getId() {
			return fId;
		}

		@Override
		public boolean equals(Object obj) {
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			final Version other = (Version) obj;
			return other.getId().equals(fId);
		}

		@Override
		public int hashCode() {
			return fId.hashCode();
		}

		public String toString() {
			return fId;
		}
	}

	/**
	 * Comparator based on the natural ordering of {@link Version}. Comparator
	 * return negative value if v1 is before v2.
	 * 
	 * @author Yi Li
	 *
	 */
	class VersionComparator implements Comparator<Version> {

		// original ordering of commits: reverse chronological order
		private List<String> order;

		public VersionComparator(List<RevCommit> history) {
			for (RevCommit h : history)
				order.add(h.getName());
		}

		@Override
		public int compare(Version o1, Version o2) {
			if (o1.getId().equals(o2.getId()))
				return 0;

			int p1 = order.lastIndexOf(o1.getId());
			int p2 = order.lastIndexOf(o2.getId());

			assert p1 != p2;

			if (p1 > p2)
				return -1;
			else
				return 1;
		}
	}
}