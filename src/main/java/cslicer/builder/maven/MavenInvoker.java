package cslicer.builder.maven;

/*
 * #%L
 * CSlicer
 *    ______ _____  __ _                  
 *   / ____// ___/ / /(_)_____ ___   _____
 *  / /     \__ \ / // // ___// _ \ / ___/
 * / /___  ___/ // // // /__ /  __// /
 * \____/ /____//_//_/ \___/ \___//_/
 * %%
 * Copyright (C) 2014 - 2015 Department of Computer Science, University of Toronto
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import org.apache.maven.shared.invoker.DefaultInvocationRequest;
import org.apache.maven.shared.invoker.DefaultInvoker;
import org.apache.maven.shared.invoker.InvocationRequest;
import org.apache.maven.shared.invoker.InvocationResult;
import org.apache.maven.shared.invoker.Invoker;
import org.apache.maven.shared.invoker.MavenInvocationException;
import org.codehaus.plexus.util.FileUtils;

import cslicer.builder.BuildScriptInvalidException;
import cslicer.builder.BuildToolInvoker;
import cslicer.builder.UnitTestScope;
import cslicer.utils.PrintUtils;

public class MavenInvoker extends BuildToolInvoker {
	private final Invoker invoker;
	private MavenPomBuilder builder;

	// private final Path workingDirectory;
	public MavenInvoker(String script) throws BuildScriptInvalidException {
		this(script, false);
	}

	public MavenInvoker(String script, boolean enableOutput)
			throws BuildScriptInvalidException {
		super(script);

		this.invoker = new DefaultInvoker();
		if (!enableOutput)
			this.invoker.setOutputHandler(null);
	}

	/**
	 * Check if a target project compiles successfully.
	 * 
	 * @return {@code true} if target project compiles
	 */
	@Override
	public boolean checkCompilation() {
		InvocationRequest request = new DefaultInvocationRequest();
		request.setPomFile(scriptPath.toFile());
		request.setInteractive(false);
		request.setGoals(Arrays.asList("compile"));

		try {
			InvocationResult res = invoker.execute(request);

			if (res.getExitCode() != 0) {
				PrintUtils.print("Compilation failed");
				return false;
			} else {
				PrintUtils.print("Compilation is successful");
				return true;
			}
		} catch (MavenInvocationException e1) {
			e1.printStackTrace();
		}
		return false;
	}

	/**
	 * Clean up build files.
	 */
	@Override
	public void cleanUp() {
		// cleaning up
		InvocationRequest request = new DefaultInvocationRequest();
		request.setPomFile(scriptPath.toFile());
		request.setGoals(Arrays.asList("clean"));

		try {
			invoker.execute(request);
		} catch (MavenInvocationException e1) {
			e1.printStackTrace();
		}

		if (builder != null)
			builder.cleanUp();
	}

	@Override
	public void restoreBuildFile() throws IOException {
		if (builder == null)
			return;
		builder.restorePomFile();
	}

	/**
	 * Run all default unit tests.
	 * 
	 * @return {@code true} if run successfully
	 */
	@Override
	public boolean runUnitTests() {
		InvocationRequest request = new DefaultInvocationRequest();
		request.setPomFile(scriptPath.toFile());
		request.setInteractive(false);
		request.setGoals(Arrays.asList("test"));

		try {
			InvocationResult res = invoker.execute(request);

			if (res.getExitCode() != 0) {
				PrintUtils.print("Test failed");
				return false;
			} else {
				PrintUtils.print("Test succeeded");
				return true;
			}
		} catch (MavenInvocationException e1) {
			e1.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean runSingleTest(UnitTestScope scope) {
		InvocationRequest request = new DefaultInvocationRequest();

		//System.out.println(scope.getMavenTestArguments());
		request.setPomFile(scriptPath.toFile())
				.setInteractive(false)
				.setGoals(
						Arrays.asList("test", scope.getMavenTestArguments(true),
								"-DfailIfNoTests=false"));

		try {
			InvocationResult res = invoker.execute(request);

			if (res.getExitCode() != 0) {
				PrintUtils.print("Test failed");
				return false;
			} else {
				PrintUtils.print("Test succeeded");
				return true;
			}
		} catch (MavenInvocationException e1) {
			e1.printStackTrace();
		}
		return false;
	}

	@Override
	public void initializeBuild(File targetPath)
			throws BuildScriptInvalidException, IOException {
		this.builder = new MavenPomBuilder(scriptPath.toFile(), targetPath);
		// add jacoco and junit plugin and surefire plugin
		execFilePath = builder.addJacocoBuildPlugin();
		builder.addJunitPlugin();
		builder.addSurefireBuildPlugin();
		// redirect build output
		classPath = builder.redirectBuildOutput();
		// copy source files
		sourceJarPath = builder.generageSourceJar();
		// disable rat check plugin
		// builder.disableRatCheck();
		// write modifications to pom file
		builder.writeToPomFile(scriptPath.toString());
	}

	@Override
	public void initializeBuild(File targetPath, String subPomPath)
			throws BuildScriptInvalidException, IOException {
		this.builder = new MavenPomBuilder(FileUtils.getFile(subPomPath),
				targetPath);
		execFilePath = builder.addJacocoBuildPlugin();
		builder.addJunitPlugin();
		builder.addSurefireBuildPlugin();
		classPath = builder.redirectBuildOutput();
		sourceJarPath = builder.generageSourceJar();
		// builder.disableRatCheck();

		builder.writeToPomFile(subPomPath);
	}
}
