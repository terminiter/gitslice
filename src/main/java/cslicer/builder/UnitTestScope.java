package cslicer.builder;

/*
 * #%L
 * CSlicer
 *    ______ _____  __ _                  
 *   / ____// ___/ / /(_)_____ ___   _____
 *  / /     \__ \ / // // ___// _ \ / ___/
 * / /___  ___/ // // // /__ /  __// /
 * \____/ /____//_//_/ \___/ \___//_/
 * %%
 * Copyright (C) 2014 - 2015 Department of Computer Science, University of Toronto
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;

/**
 * Used to specify which unit tests to run. Now three modes are supported: 1.
 * Run a collection of test classes 2. Run all test classes 3. Run a specific
 * test method in a test class
 * 
 * (subject to future extension)
 * 
 * @author liyi
 *
 */
public class UnitTestScope {
	private Map<String, Collection<String>> includedTests;
	private Map<String, Collection<String>> excludedTests;

	public UnitTestScope() {
		includedTests = new Hashtable<String, Collection<String>>();
		excludedTests = new Hashtable<String, Collection<String>>();
	}

	public UnitTestScope includeTest(final String className) {
		includedTests.put(className, new ArrayList<String>());
		return this;
	}

	public UnitTestScope includeTest(final String className,
			final String methodName) {
		if (includedTests.containsKey(className))
			includedTests.get(className).add(methodName);
		else
			includedTests.put(className, Arrays.asList(methodName));

		return this;
	}

	public UnitTestScope excludeTest(final String className) {
		excludedTests.put(className, new ArrayList<String>());
		return this;
	}
	
	public UnitTestScope excludeTest(final String className,
			final String methodName) {
		if (excludedTests.containsKey(className))
			excludedTests.get(className).add(methodName);
		else
			excludedTests.put(className, Arrays.asList(methodName));

		return this;
	}

	public String getMavenTestArguments(boolean isInclude) {

		ArrayList<String> testClasses = new ArrayList<String>();
		Map<String, Collection<String>> tests = isInclude ? includedTests
				: excludedTests;

		for (Entry<String, Collection<String>> test : tests.entrySet()) {
			if (test.getValue().isEmpty()) {
				testClasses.add(test.getKey());
				continue;
			}

			testClasses.add(test.getKey() + "#"
					+ StringUtils.join(test.getValue(), "+"));
		}

		return "-Dtest=" + StringUtils.join(testClasses, ",");
	}

	public boolean includeAllTest() {
		return includedTests.isEmpty();
	}
	
	public boolean hasExcludedTest() {
		return !excludedTests.isEmpty();
	}
}
