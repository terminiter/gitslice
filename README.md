![cslicer.png](https://bitbucket.org/repo/Een7Xx/images/3832351779-cslicer.png)

This is the source directory of **CSlicer**, an automatic semantic slicing tool for Java projects hosted in Git repositories.

---

## License

The license is in LICENSE.txt

## Introduction

CSlicer is a semantic slicing tool for Git reposiotries under active development by the Software
Engineering group at the University of Toronto.
The goal of semantic slicing is to identify a subset of change sets (commits) that implementing a software functionality (e.g., feature, enhancement, or bug fix). The applications include code migration, branch refactoring, etc.
CSlicer currently works only for Java repositories.

## Dependencies

* Maven  
[Apache Maven](http://maven.apache.org) is used for dependency control. To build the project, first download and install Maven following the instructions [here](http://maven.apache.org/download.cgi) (version >= 3.2). Make sure environment variables ```M2_HOME```, ```M2```, and ```JAVA_HOME``` are set correctly according to the instructions.
All required dependencies will be fetched and installed by Maven automatically.

* ChangeDistiller  
We use [ChangeDistiller](https://bitbucket.org/sealuzh/tools-changedistiller/wiki/Home) for categorizing and analyzing changes.
A local copy of ChangeDistiller-SNAPSHOT-0.0.1 is included in the lib directory.

* JavaSlicer  
We use [JavaSlicer](https://github.com/hammacher/javaslicer) for dynamic slicing and computing checked coverage. A local copy of JavaSlicer is included in the lib directory.

## Install CSlicer

Requirements:

* JDK 1.7+
* Maven 3.0 or later
* Internet connection for first build (to fetch all Maven and CSlicer dependencies)

Get the source and external dependencies
```
git clone https://liyistc@bitbucket.org/liyistc/gitslice.git cslicer
cd cslicer
git submodule init
git submodule update
```

Maven build goals:

* Clean: ```mvn clean```
* Compile: ```mvn compile```
* Run tests: ```mvn test```
* Create JAR: ```mvn package```
* Install JAR in M2 cache: ```mvn install```
* Build javadocs: ```mvn javadoc:javadoc```

Build options:

* Use ```-DskipTests``` to skip tests

## Run CSlicer ##

Find executable jar files at ```cslicer/target``` and all dependencies at ```cslicer/target/lib```. Run CSlicer using
 
```
java -jar cslicer-XXX-jar-with-dependencies.jar -c <CONFIG_FILE_PATH>
```

The target project configuration file specifies the location, target commits (history), path to source code root, etc. An example of a configuration file (with extension .properties) is as follows.

```
#!properties
# This is an example project configuration file project.properties
# Path to target repository (required)
repoPath = /home/liyi/bit/elasticsearch/.git
# Path to Jacoco exec dump file (required)
execPath = /home/liyi/Dropbox/Ideas/dep/testdata/elastic/e3/jacoco.exec
# History ending commit (required)
endCommit = 220842a1b01fe39ee264d38262fdaa831bf11def
# Target history length (optional)
historyLength = 50
# History starting commit (optional); 
#if endCommit and historyLength are both provided 
# then startCommit will be ignored
startCommit = 220842a1b01fe39ee264d38262fdaa831bf11def
# Path to the root directory of source code (required)
sourceRoot = /home/liyi/bit/elasticsearch/src/main/java
# Path to the root directory of class files (required)
classRoot = /home/liyi/bit/elasticsearch/target/classes
# Path to the build script file (optional); 
# if project is using Maven, then CSlicer is able to 
# build and run tests automatically
buildScriptPath = /home/liyi/bit/elasticsearch/pom.xml
# Path to output touch set file (optional)
touchSetPath = /tmp/touch-e3.txt
```

See detailed usage by entering the command

```
java -jar cslicer-XXX-jar-with-dependencies.jar -h
```

## Publications ##

[Semantic Slicing of Software Version Histories](http://www.cs.toronto.edu/~liyi/host/files/ase15.pdf)  
Yi Li, Julia Rubin, and Marsha Chechik  
In proceedings of the 30th IEEE/ACM International Conference on
Automated Software Engineering (ASE 2015)